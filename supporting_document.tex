\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[a4paper, total={6in, 8in}]{geometry}
\usepackage{multicol}
\usepackage{hyperref}
\usepackage{url}
\usepackage{titling}

\setlength{\droptitle}{-10em}
\title{TOPPS: Traffic Optimization via Performance-based Path Selection}
\author{Thorben Krüger, David Hausheer}
%\subtitle{Completing the picture of a SCION-based Next-Generation Path-Aware Internet by addid configurable support for automatic traffic optimization and intelligent path choice to the edge.}
\begin{document}
\maketitle

\begin{multicols}{2}
%[
%\section{First Section}
%All human things are subject to decay. And when fate summons, Monarchs must obey.
  %]
  \begin{abstract}
    The TOPPS project enables a Next-Generation Path Aware Internet by providing
    configurable support for automatic traffic optimization and intelligent path
    choices to bring the Internet to a new level of efficiency, scalability,
    security, and resilience.

    Novel applications in the area of edge-computing, tactile internet, and
    autonomous robotics are creating new optimization challenges in
    communication networks. Emerging path-aware networking (PAN) approaches like
    SCION enable Internet hosts to choose from a number of available paths to a
    given destination. What is still lacking is a way to actually leverage these
    path options for the benefit of the users.

    The TOPPS project aims to fill this gap by optimizing path selection in PAN
    to enhance the communication for the different applications of any host,
    balancing potentially conflicting requirements such as bandwidth, latency,
    and reliability.

    Our project will design and implement a next-generation API to a novel
    middleware layer that supports the actual path choice for the user,
    leveraging performance metrics and other information about network
    conditions to automatically perform performance optimization, improved
    traffic engineering (e.g. load balancing, failure recovery) and multipath
    optimization.

      %% As our end-host based path-selection scheme ultimately aims to supersede
      %% traditional, distributed network routing mechanisms, we also account for
      %% its global impact on network utilization and congestion. % FIXME

      %% The notion of ``path properties'' (currently being formalized in PANRG at
      %% the IRTF) provides a useful conceptual framework for reasoning about this
      %% requirement, which the greedy heuristics of current primitive path-selection
      %% schemes systematically fail to address.

      %% We will also closely track the work done in the TAPS working group at the
      %% IETF, which is aiming to overcome the limitations the traditional socket
      %% API. Application interaction with network communication is to be
      %% modernized, and our project could provide the first litmus test for the
      %% proposed new API standard in a SCION context. This could also significantly
      %% help SCION to some day ``officially'' coexist with TCP/IP and QUIC/IP on
      %% end hosts.


  \end{abstract}


  \section*{Introduction}

  Networking architectures based on packet-carried forwarding state like
  SCION~\cite{scion} promise to support practical multipath communication with
  increased availability and transparent failover to alternate paths. Such
  properties are highly desirable for a more resilient Internet architecture and
  promise to be a strong incentive for ISPs to maintain and advertise redundant
  uplinks to higher-tier networks, allowing their customers to autonomously
  react to potential upstream failures, bottlenecks and other
  events~\cite{scion}. On the other hand, traffic distribution patterns across a
  given network will predominantly be influenced by path-choices of the sources,
  with potential negative impact on network performance and operational costs.
  Such undesirable outcome can be the result of overly naive behavior by the
  path-selecting hosts. If prevalent network properties are not taken into
  account when paths are chosen, network links can become overloaded and choked,
  resulting in congestion.\\ %Other detrimental
  %effects include ...

  Current, na\"ive path-selection schemes in path aware networking (PAN)
  architectures like SCION simply select the shortest paths from a set of
  options~\cite{scion}. While this is a reasonable default, sub-optimal outcomes
  can be foreseen for a number of scenarios. Even paths of equal length can
  differ in their suitability for a given network-communication task. Such
  differences can be stated in terms of \textit{path properties}, which are
  objective traits that can be measured or assessed. Efforts are underway at the
  IETF to develop a suitable vocabulary to reason about these
  concepts~\cite{irtf-panrg-path-properties-00}.\\

  If path-selection authority resides with the end-hosts, then end-hosts require
  meaningful access to the properties of each of the paths they could select.
  For the current purposes of our proposed system, each host is assumed to
  gather its own metrics about network conditions from its own vantage point. In
  a future evolution of the system, additional property information can be
  provided by dedicated Internet services that can be queried automatically.\\

  %% \subsection*{The Path-Aware SCION Architecture}

  %% The open source SCION Architecture offers path-awareness on inter-network
  %% granularity. This is a core principle of its design. It cryptographically
  %% guarantees that any network on the chosen path can only be entered and exited
  %% via the exact interfaces specified in the packet header for that network. 
\newpage

  \subsection*{The TOPPS Vision}

  %% The open source SCION Architecture 

  Our proposed system is envisioned to be an integral part of the networking
  stack of any next-generation Internet device. It will expose a high-level
  networking API with novel features to application programmers. Telepresence
  applications will be able to express a preference for paths with lower latency
  while uploads or downloads can automatically be moved to paths offering the
  highest bandwidth. Critical functionality like a call to an emergency hotline
  will be able to utilize multiple paths for redundancy and fault tolerance.\\

  At the back-end, our system will translate these high-level preferences into
  a set of sophisticated network-level interactions:\\

  Measured/observed performance data from all current (and recent) SCION
  connections are fed into \textit{Pathfinder}, our ``path quality estimation
  engine'' that is central to each host. Given certain requirements, this engine
  can aid in path selection for new connections. Whenever an application needs
  to establish a new connection, the back-end consults \textit{Pathfinder} for a
  probable best choice given the application's preferences. When no relevant or
  recent information is available for some or all of the potential paths, it
  simultaneously tries to establish a connection over every path using a
  ``connection racing'' approach similar to the one detailed in~\cite{rfc6555}.
  The completion times of the different handshakes give a first indication about
  the latencies on each respective path and serve as initial data points to be
  entered into the path quality database.\\

  The system will occasionally migrate non-critical connections to alternative
  paths while closely monitoring the associated performance. The change is
  immediately reversed if the relevant communication metrics do not improve.
  Otherwise, a better path has likely been discovered that could subsequently
  also be used for more critical communication.\\

  \subsubsection*{Example:}
  A host selects an initial (short) path at random and use it for
  communication while monitoring the round trip time of the path. In an
  exploratory fashion, or when the latency is unacceptably high, the host reacts
  on this information by switching to a known alternate path and compare the
  newly observed delay. It later also uses the recently measured latency value as
  grounds for a performance estimate when reusing the path for a new connection.\\

  %% By itself, this ``isolated'' approach already provides a good
  %% baseline. It makes particular sense when the host is acting as a
  %% \textit{server} that typically manages some number of parallel connections to
  %% a localized set of \textit{clients} from the same few access networks, i.e.,
  %% when the ratio of concurrent connections per destination network is large
  %% enough to allow for permanent monitoring.


  This and similar other techniques combine to achieve improved network
  utilization from the perspective of the host as well as from the perspective
  of the whole network. Exploratory path selection on the hosts is likely to
  discover under-utilized paths, ultimately leading to a much more balanced
  network utilization.

  \subsection*{Structure of this Proposal}
  The remainder of this proposal is structured as follows: \begin{itemize}
  \item In \nameref{background}, we give an overview about the concepts and terminology behind
    \begin{itemize}
    \item \nameref{awareness}
    \item \nameref{choice}
    \item \nameref{properties}
    \end{itemize}
  \item In \nameref{outcome}, we develop a notion of the requirements for our system
  \item These considerations inform our concept of \nameref{process} and \nameref{processing}
  \item Finally, the high level \nameref{summary} consolidates our proposal into a coherent picture and outlook
  \end{itemize}

\newpage

  \section*{Background}
  \label{background}

  In this section, we introduce core concepts and terminology that are relevant to the proposed TOPPS system.

  \subsection*{Path Awareness}
  \label{awareness}
  As a major next-generation Internet architecture, the SCION~\cite{scion} project is built around core concepts of path awareness as a way to provide
  \begin{itemize}
  \item rapid failover
  \item easy multipath
  \item geofencing
  \end{itemize}
  In IETF terminology~\cite{irtf-panrg-path-properties-00}, a \textit{path} is a
  collection of adjacent \textit{path elements}. A path element can mean a
  \textit{host} (client or server), a \textit{link} (physical or virtual), or
  some other (generic) entity, called a \textit{node} that processes packets.

  \subsection*{Path Choice}
  \label{choice}
  A path aware network can only provide an advantage over classic routing
  methods if path awareness also means path choice. \textit{Path awareness}
  should inherently imply the capacity for \textit{path choice}, which is the
  necessary prerequisite to achieve useful failure tolerance, multipath
  communication and improved network utilization. Consequently, there should
  always exist at least two separate paths between any source and destination.\\

  It is the responsibility of the network (e.g., ISP) to provide its hosts with
  reasonable Path options\footnote{``Path options'' do not necessarily mean
    ``ready made paths''. In SCION, path servers disseminate ``path segments''
    from which the hosts then construct complete paths.}, while final path
  \textit{selection} authority lies with the hosts.\\

  In case of asymmetric communication (i.e., when a return path does not equal
  the reverse path), path constraint negotiation might be necessary between both
  ends (i.e., between client-hosts and server-hosts).\\

  Two different paths between the same two hosts can also differ significantly
  in their suitability for the actual communication requirements. The specific
  suitability of a path can be assessed through its \textit{properties}.

  \subsection*{Path Properties}
  \label{properties}
  In IETF terminology~\cite{irtf-panrg-path-properties-00}, a property may refer
  to one or more elements of a path. In this proposal, we are primarily concerned
  with \textit{path properties} which apply to paths as a whole. A path property
  often is an \textit{aggregated property}, which reduces a larger collection of
  properties to a single value. For instance, the MTU of a path is the minimum
  MTU of all links on the path.\\

  \textit{Observed properties} are directly measured, e.g., the round trip delay
  across a path can be observed.\\

  \textit{Assessed properties} are approximations of a value, e.g., the
  (re)usage of a recently observed property as estimation for future
  performance. An assessed property should include some notion of the
  reliability of the assessment.\\

  %% For the purposes of this proposal, we will restrict ourselves to properties that
  %% have significant impact on quality of experience (QoE) for the users, since we
  %% expect these to be the main factor in path choice.\\

  Extending the terminology, we will also distinguish
  \begin{itemize}
  \item \textbf{transient properties} (e.g., congestion/queueing delay, link
    usage): often, these properties will reflect assessments of fluctuating reliability.
  \item \textbf{long-lived properties} (e.g., propagation delay, link
    capacities, MTU): general properties that stay constant for prolonged
    periods of time but have impact on desirability of a path
  \end{itemize}

  \subsection*{Related Work}

   Some of the issues that this proposal explores can be seen as a more generic
   form of the p2p-centric ``Application-Layer Traffic Optimization (ALTO)
   Problem Statement'' explored in informational RFC 5693~\cite{rfc5693}.

   Research about P2P Networks is looking at the related problem of overlay
   networks that do not account for underlying network topologies. The resources
   offered by p2p systems often exist in multiple replicas~\cite{rfc5693},
   i.e., they are accessible across different paths. Path selection in P2P
   overlay networks very rarely takes underlying network topology into account.
   This results in inefficient traffic patterns on the network.
  ~\cite{aggarwal2007can} proposes an ``oracle'' mechanism that could be
   deployed by ISPs to improve performance.

  \section*{Desired Outcome of Path Selection}
  \label{outcome}

  Consider two hosts, a client (c) and a server (s), who wish to communicate. They need to
  find a set of paths that satisfies the following criteria:
  \begin{itemize}
  \item a minimum number of alternative paths (enabling failover or multipath)
  \item optimizing for some (application specific) property (e.g., low latency
    or high throughput)
  \item for multipath: each path requires similar latency (disregarding
    special cases where this is not a requirement)
  \item for failover: highly disjoint paths for failure isolation. (needs to
    take possible underlay properties into account)
  \item all paths need to satisfy possible constraints of either party (e.g.,
    geofencing\footnote{A client may wish for its traffic to not leave the
      country or legislative domain or to avoid a particular network. Such
      demands are typically summarized under the term ``geofencing''.})
  \end{itemize}
  %% With such a set of paths, the hosts can then engage in communication, with no
  %% detriment to other participants on the network.\\

  %% On the current Internet, we can observe certain systematic asymmetries in the
  %% flow of information between hosts. On the web in particular, a client
  %% typically sends e.g., a comparatively short HTTP request to a server. The web
  %% server sends a reply with the requested content, which may consist of orders
  %% of magnitude more bytes than the original request.\footnote{Internet Service
  %%   Providers can limit their customers' usable upstream bandwidth to about a
  %%   10th of the advertised downstream bandwidth without too many complaints. The
  %%   consumer-grade Internet-access technology ADSL even explicitly relies on
  %%   this observation on a technical level.}

  %% If two hosts, a client and server, engage in such asymmetric communication, it
  %% is therefore of much greater importance to optimize the path from server to
  %% client than the path across which only the request is sent. The benefits from
  %% a good path choice accumulate over time.

  %% The path from server to client may of course simply be the reverse of the
  %% path from client to server. In such a scenario, it exclusively falls to the
  %% client to pick a good path on behalf of the server. The client may actually
  %% wish this in case it has certain demands of its paths that the server would
  %% be unlikely to honor in its reply.
   
   
   The path selection process needs to account for possibly conflicting criteria
   that the different parties might have. Consumers/clients/users request data from
   content/information/service providers. Clients typically send short requests,
   while the server replies typically are orders of magnitude larger. In other
   words, the flow of information (i.e., ``traffic'') is typically asymmetric.

   \subsection*{Client Perspective}
   A client-host is usually the initiator of any communication with servers on
   the Internet on behalf of a (human) user. The user desires a good Quality of
   Experience (QoE), which translates to high-level demands for interaction with
   the network, that may vary with the current task or goal. In addition, some
   policy may be in effect on a user's behalf that, e.g., ensures that her
   traffic does not flow across certain undesirable networks. 
   
   On the client side, some important considerations for path selection include:
   \begin{itemize}
   \item geofencing
   \item fault tolerance
   \item automatically good QoE (minimal user interaction, keyword: sane defaults)
   \end{itemize}
   
   \subsection*{Server Perspective}
   A server-host typically hosts some form of data (e.g., ``content'') that is
   of interest to clients. The latter issue requests for this data to the server
   and expect delivery of the content in a way that satisfies their demands for
   QoE. In most cases, the satisfaction of a request or query constitutes an
   actively \textit{desired} interaction on behalf of the server operators. The
   fact that a user is interested in the content on offer is often part of the
   business model. As a consequence, providing good QoE for the user is
   important. Otherwise they might decide to interact with the competition in
   future~\cite{espresso}.

   \subsection*{Network Perspective}
   The network as a whole has little influence on the concrete choice of paths
   that client and server hosts negotiate. However, good network resource
   utilization is in the interest of all parties. On the host-side, there is
   incentive to find paths with unused resources to maximize performance. On the
   network side, better utilization of otherwise under-used paths is equivalent
   to better balanced traffic patterns and less chance for congestion.

   \newpage

   \section*{The Path Selection Process}
   \label{process}
   Recall that, in a PAN like SCION~\cite{scion}, every host, no matter whether
   client or server, can do its own path selection. Depending on the
   type of host, there are different perspectives on this process.
   
   \subsection*{Client Perspective}
   The client has limited information about the current network properties on
   the potential paths to a destination. Without a dedicated way to query for
   path quality, the client can not easily select paths with good QoE. It can
   only send its queries in a best-effort way, since the path properties are not
   known in advance. Some cached quality information might still be available
   from past connections. However, validity of such information decays with time
   as network utilization patters change.

   \subsection*{Server Perspective}

   The server potentially has path quality information from concurrent flows
   into the same access networks that it could leverage. KPIs like RTT and
   bandwidth for different return paths into a given access network can be kept
   track of. Flows could be switched away from paths with degraded KPIs.

   \subsection*{Combined Client-Server Path Negotiation}
   One of the long-term aims of our system is to explicitly allow client and
   server to negotiate their path choice preferences. Both parties typically
   meet in the shared interests of trying to provide good QoE, which should
   result in quick agreement on a path that reflects a suitable compromise.

   \vspace{2cm}
   \section*{Path Property Processing}
   \label{processing}
   Path properties form the basis for an informed path selection decision. In
   this section, we detail relevant techniques to gather and process such
   information to assist with the final path choice.
   
   \subsection*{Property Collection}
   The principal method to obtain property information is through passive
   measurements on end hosts. Here directly observed path performance on active
   flows give indications about metrics such as \textit{bandwidth,
     round-trip-time} and \textit{jitter}.\\

   Complementary measurements also take place at a larger scale on network
   ingress and egress. Network equipment can additionally also monitor, e.g.,
   \textit{queue lengths} and \textit{link utilization}.

  \subsection*{Property Processing}
  Hosts rely on locally collected path property information to inform their path
  choice. Additional information that has been obtained at the network level can
  optionally be incorporated in the final decision. By default, the path
  selection process does not rely on the presence of any dedicated network-level
  property services. Purely host-based path property assessments form the core
  of our bottom-up approach, which can later be extended by an optional, network-level
  service that can also take additional information into account.\\
  
  Aggregating observed and assessed properties through specialized network
  services that also orchestrate dissemination across the inter-domain is the
  envisioned future direction after the TOPPS core functionality has
  sufficiently matured. Together with the host-based approach, which needs to
  come first, this can then be combined into a hybrid path selection strategy.

  %% \subsection*{Host-based, passive measurements}
  %%  Some hosts (e.g., servers) manage a large number of concurrent flows into a
  %%  given access network. This can easily allow them to monitor the performance
  %%  of each possible path into that network. A modification of the
  %%  REPLEX~\cite{replex} algorithm can be used to distribute flows.

\newpage
   \section*{Summary}
   \label{summary}

   \subsection*{Design Goals}
   Our proposed TOPPS system aims to leverage the novel opportunities of a path
   aware network architecture to maintain an up-to-date local view of current
   network properties. Based on this information, path choice is conducted
   according to application requirements. The system tracks performance on a
   per-path granularity, monitoring established connections using a range of
   metrics. This information feeds back into the path selection process,
   allowing for progressive refinement of the local view on the network, leading
   to progressively better path choices. In an exploratory fashion, non-critical
   communication is sometimes experimentally migrated to promising, unknown
   paths to gain rough estimates of their performance. This way, paths with
   improved properties can be discovered with marginal risk.\\

   While the system is envisioned as a host-centric service, its effects on the
   larger network are anticipated to be beneficial. Interests of individual
   hosts as well as the network as a whole align in terms of network utilization.

   \subsection*{Future Improvements}
   Once the core TOPPS system has matured, the path selection process can be
   further refined through shared information from other hosts and
   the network as a whole. A host can ``donate'' its derived path quality
   information to a path property service, that can e.g., be realized as a
   secondary function of SCION path servers or in a stand-alone fashion. This is
   to be designed in a way that it has a chance to become ``best practice'' and
   therefore the default behavior of the SCION reference implementation. Path
   servers can encode external path-quality estimates in the ordering of their
   responses and (optionally) provide explicit properties in additional payload
   fields. We envision a tit-for-tat approach for this: to ensure that the path
   quality services gather enough ``data donations'', they only provide path
   quality estimates to a host after having received a certain amount of
   reasonable values as ``donations'' from it. As a result, it can become common
   practice for a host freshly coming online to perform certain automatic probes
   in order to gain that privilege as quickly as possible.



%
\bibliographystyle{plain}
\bibliography{bibliography/bibliography}



\end{multicols}
 
\end{document}

